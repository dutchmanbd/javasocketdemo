package com.ticonsys.main;

import com.ticonsys.client.MySocketClient;
import com.ticonsys.model.Message;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter session id: ");
        String sessionId = sc.nextLine();
        System.out.println("Enter user id");
        String userId = sc.nextLine();
        MySocketClient mySocketClient = MySocketClient.getInstance(sessionId, userId);
        mySocketClient.observeOnConnectionEvent();
        mySocketClient.observeOnMessageEvent();
        Message message;
        while(true){
            System.out.println("Are you want show list (y/n):");
            String yesNo = sc.nextLine();
            if(yesNo.equals("y") || yesNo.equals("Y")){
                mySocketClient.showUserList();
            }
            System.out.println("Enter To: ");
            String to = sc.nextLine();
            System.out.println("Enter message:");
            String messageText = sc.nextLine();

            message = new Message();
            message.setMessageFrom(userId);
            message.setMessageTo(to);
            message.setMessageType("sample");
            message.setMessageBody(messageText);
            message.setOptional("optional");

            mySocketClient.sendMessage(message);
        }

    }
}
