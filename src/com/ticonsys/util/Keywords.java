package com.ticonsys.util;

public final class Keywords {

    public static final String KEY_SESSION_ID = "session_id";
    public static final String KEY_FROM = "from";
    public static final String KEY_TO = "to";
    public static final String KEY_MESSAGE_TYPE = "message_type";
    public static final String KEY_MESSAGE_BODY = "message_body";
    public static final String KEY_OPTIONAL = "optional";
    public static final String KEY_USER_ID = "user_id";
}
