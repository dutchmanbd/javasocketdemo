package com.ticonsys.util;

public final class Constant {

//    public static final String BASE_URL = "http://3.36.52.185";
    public static final String BASE_URL = "https://vboard.wmeeting.live";



    public static final String EVENT_ROOM_MESSAGE = "room_msg_";
    public static final String EVENT_USER_MESSAGE = "user_msg_";
    public static final String EVENT_ACTIVE_USERS = "active_users";
    public static final String EVENT_SEND_MESSAGE = "msg_input";


}
