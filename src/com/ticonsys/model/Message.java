package com.ticonsys.model;

import com.google.gson.annotations.SerializedName;

import static com.ticonsys.util.Keywords.*;

public class Message {

    @SerializedName(KEY_FROM)
    String messageFrom;

    @SerializedName(KEY_TO)
    String messageTo;

    @SerializedName(KEY_MESSAGE_TYPE)
    String messageType;

    @SerializedName(KEY_MESSAGE_BODY)
    String messageBody;

    @SerializedName(KEY_OPTIONAL)
    String optional;

    public Message() {

    }

    public Message(String messageFrom, String messageTo, String messageType, String messageBody, String optional) {
        this.messageFrom = messageFrom;
        this.messageTo = messageTo;
        this.messageType = messageType;
        this.messageBody = messageBody;
        this.optional = optional;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(String messageTo) {
        this.messageTo = messageTo;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getOptional() {
        return optional;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }

    @Override
    public String toString() {
        return "Message{" +
                ", messageFrom='" + messageFrom + '\'' +
                ", messageTo='" + messageTo + '\'' +
                ", messageType='" + messageType + '\'' +
                ", messageBody='" + messageBody + '\'' +
                ", optional='" + optional + '\'' +
                '}';
    }
}
