package com.ticonsys.client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ticonsys.model.Message;
import com.ticonsys.model.User;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.List;

import static com.ticonsys.util.Constant.*;

public class MySocketClient {

    private Socket mSocket;
    private final String userId;
    private final String sessionId;
    private final Gson gson;
    private List<User> users;


    private static MySocketClient mySocketClient;

    public static MySocketClient getInstance(
            String sessionId,
            String userId
    ) {
        if (mySocketClient == null) {
            mySocketClient = new MySocketClient(sessionId, userId);
        }
        return mySocketClient;
    }


    private MySocketClient(
            String sessionId,
            String userId
    ) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.gson = new Gson();
        try {
            IO.Options options = new IO.Options();
            options.query = "user_id="+userId+"&session_id="+sessionId;
            this.mSocket = IO.socket(BASE_URL, options);
            connect();
        } catch (URISyntaxException e) {
            System.out.println(e.getMessage());
        }
    }

    private void connect() {
        if (mSocket != null) {
            mSocket.connect();
        }
    }

    /**
     * Socket Default observers
     */
    public void observeOnConnectionEvent() {
        /**
         * When successfully connected to socket
         * EVENT_CONNECT is trigger by Socket client
         */
        mSocket.on(Socket.EVENT_CONNECT, data -> {
            try {
//                System.out.println("Connection is connected: "+data);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });

        /**
         * For connection error
         */
        mSocket.on(Socket.EVENT_CONNECT_ERROR, data -> {
            System.out.println("Connection error");
        });

        /**
         * For connection disconnect
         */
        mSocket.on(Socket.EVENT_DISCONNECT, data -> {
            System.out.println("Disconnect");
        });

        /**
         * For connection timeout
         */
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, data -> {
            System.out.println("Connection Timeout");
        });

        /**
         * For error event
         */
        mSocket.on(Socket.EVENT_ERROR, data -> {
            System.out.println("Error");
        });

    }

    public void showUserList(){
        if(users != null && !users.isEmpty()){
            StringBuilder sb = new StringBuilder();
            sb.append("users: ");
            for(User user : users){
                sb.append(user.getUserId()).append(",");
            }
            System.out.println(sb.toString());
        }
    }

    /**
     * Observe Custom event
     */
    public void observeOnMessageEvent() {

        /**
         * When user join to the session update active users list
         */
        mSocket.on(EVENT_ACTIVE_USERS, args -> {
            try {

                Type listType = new TypeToken<List<User>>(){}.getType();
                users = gson.fromJson(args[0].toString(), listType);
//                System.out.println("users: "+users);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });


        /**
         * When any users send message using EVENT_SEND_MESSAGE for every one(that means message to field is empty)
         * then this event is triggered
         */
        mSocket.on(EVENT_ROOM_MESSAGE + sessionId, args -> {
            try {
                Message message = gson.fromJson(args[0].toString(), Message.class);
                if (!userId.equals(message.getMessageFrom())) {
                    System.out.println("Received from " + message.getMessageFrom() + " message: " + message.getMessageBody());
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });

        /**
         * When any users send message using EVENT_SEND_MESSAGE for you then
         * then this event is triggered
         */
        mSocket.on(EVENT_USER_MESSAGE + userId, args -> {
            try {
                Message message = gson.fromJson(args[0].toString(), Message.class);
                if (!userId.equals(message.getMessageFrom())) {
                    System.out.println("Received from " + message.getMessageFrom() + " message: " + message.getMessageBody());
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        });

    }

    /**
     * Using this message you can send message to server
     *
     * @param message
     */
    public void sendMessage(Message message) {
        try {
            /**
             * emit new message here
             */
            mSocket.emit(EVENT_SEND_MESSAGE, gson.toJson(message));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


}
